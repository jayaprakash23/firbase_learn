var ui = new firebaseui.auth.AuthUI(firebase.auth());

var uiConfig = {
    signInSuccessUrl: '/',
    callbacks: {signInSuccess: true},
    signInOptions: [
        {
            provider:firebase.auth.EmailAuthProvider.PROVIDER_ID,
            requireDisplayName: false
        },
        firebase.auth.GoogleAuthProvider.PROVIDER_ID,
        firebaseui.auth.AnonymousAuthProvider.PROVIDER_ID,
        {
            provider: firebase.auth.PhoneAuthProvider.PROVIDER_ID,
            recaptchaParameters: {size: 'invisible'},
        }
    ],
};

ui.start('#firebaseui-auth-container', uiConfig);
if (ui.isPendingRedirect()) {ui.start('#firebaseui-auth-container', uiConfig);}

var handleSignedInUser = function(user) {
    console.log('LOOOOOGGGG');
    document.getElementById('user-signed-in').style.display = 'block';
    document.getElementById('user-signed-out').style.display = 'none';
    document.getElementById('name').textContent = user.displayName ? user.displayName : user.phoneNumber;
    document.getElementById('email').textContent = user.email;
    document.getElementById('phone').textContent = user.phoneNumber;
    if (user.photoURL) {
        var photoURL = user.photoURL;
        // Append size to the photo URL for Google hosted images to avoid requesting
        // the image with its original resolution (using more bandwidth than needed)
        // when it is going to be presented in smaller size.
        if ((photoURL.indexOf('googleusercontent.com') != -1) ||
            (photoURL.indexOf('ggpht.com') != -1)) {
            photoURL = photoURL + '?sz=' +
                document.getElementById('photo').clientHeight;
        }
        document.getElementById('photo').src = photoURL;
        document.getElementById('photo').style.display = 'block';
    } else {
        document.getElementById('photo').style.display = 'none';
    }
    nameinDB = usersRef.where("name","==",user.name);
    emailinDB = usersRef.where("name","==",user.email);

    console.log(nameinDB);
    console.log(emailinDB);
    console.log("bort");

};

var handleSignedOutUser = function() {
    document.getElementById('user-signed-in').style.display = 'none';
    document.getElementById('user-signed-out').style.display = 'block';
    ui.start('#firebaseui-auth-container', uiConfig);
};

firebase.auth().onAuthStateChanged(function(user) {
    document.getElementById('loading').style.display = 'none';
    document.getElementById('loaded').style.display = 'block';
    user ? handleSignedInUser(user) : handleSignedOutUser();
});

function recaptchaVerifierInvisible() {
    function onSignInSubmit() {
      // TODO(you): Implement
    }
    window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('sign-in-button', {
      'size': 'invisible',
      'callback': (response) => {
        // reCAPTCHA solved, allow signInWithPhoneNumber.
        onSignInSubmit();
      }
    });
  }
  

 var initApp = function() {
    document.getElementById('sign-out').addEventListener('click', function() {
        console.log('clicked log out');
        firebase.auth().signOut();
    });
};

window.addEventListener('load', initApp);
window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('sign-in-button', {
  'size': 'invisible',
  'callback': (response) => {onSignInSubmit();}
});

//DB 
console.log(auth);